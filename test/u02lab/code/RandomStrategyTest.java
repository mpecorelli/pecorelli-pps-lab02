package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by margherita on 07/03/17.
 */
public class RandomStrategyTest {

    private static final int NUMBER_OF_BITS = 10;
    private static final int NEXT_DONE = 4;

    private SequenceGenerator randomGenerator;

    @Before
    public void begin() {
        this.randomGenerator = new GeneratorFactoryImpl().createRandomGenerator(NUMBER_OF_BITS);
    }

    @Test
    public void isABinaryNumber() throws Exception {
        for(int i = 0; i < NUMBER_OF_BITS; i++) {
            Optional optional = this.randomGenerator.next();
            assertTrue(optional.equals(Optional.of(1)) || optional.equals(Optional.of(0)));
        }
        assertEquals(Optional.empty(), this.randomGenerator.next());
    }

    @Test
    public void doesResetTurnBackToStart() throws Exception {
        int i = this.randomGenerator.next().get();
        this.randomGenerator.next();
        this.randomGenerator.next();
        this.randomGenerator.reset();
        assertEquals(Optional.of(i), this.randomGenerator.next());
    }

    @Test
    public void isOverJustAfterTheEnd() throws Exception {
        assertFalse(this.randomGenerator.isOver());
        this.randomGenerator.next();
        assertFalse(this.randomGenerator.isOver());
        for(int i = 0; i < NUMBER_OF_BITS - 1; i++){
            this.randomGenerator.next();
        }
        assertTrue(this.randomGenerator.isOver());
    }

    @Test
    public void doesReturnsAllRemainingBits() throws Exception {
        for(int i = 0; i < NEXT_DONE; i++){
            this.randomGenerator.next();
        }
        assertEquals(NUMBER_OF_BITS - NEXT_DONE, this.randomGenerator.allRemaining().size());
    }

}