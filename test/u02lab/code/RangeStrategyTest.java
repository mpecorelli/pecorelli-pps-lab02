package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by margherita on 07/03/17.
 */
public class RangeStrategyTest {

    private static final int START = 3;
    private static final int STOP = 5;

    private SequenceGenerator rangeGenerator;

    @Before
    public void begin() {
        this.rangeGenerator = new GeneratorFactoryImpl().createRangeGenerator(START,STOP);
    }

    @Test
    public void isNextNumberCorrect() throws Exception {
        for (int i = START; i <= STOP; i++) {
            assertEquals(Optional.of(i),this.rangeGenerator.next());
        }
    }

    private void toTheEnd(final int from, final int to) {
        for(int i = from; i <= to; i++) {
            this.rangeGenerator.next();
        }
    }

    @Test
    public void isEmptyAfterStop() throws Exception {
        this.toTheEnd(START,STOP);
        assertEquals(Optional.empty(),this.rangeGenerator.next());
    }

    @Test
    public void doesResetTurnBackToStart() throws Exception {
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        this.rangeGenerator.reset();
        assertEquals(Optional.of(START),this.rangeGenerator.next());
    }

    @Test
    public void isOverJustAtTheEnd() throws Exception {
        assertFalse(this.rangeGenerator.isOver());
        this.rangeGenerator.next();
        assertFalse(this.rangeGenerator.isOver());
        this.toTheEnd(START+1,STOP);
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void givesAllRemainingNumbers() throws Exception {
        List<Integer> remaining = new ArrayList<>();
        for (int i = START + 1; i <= STOP; i++) {
            remaining.add(i);
        }
        this.rangeGenerator.next();
        assertEquals(remaining, this.rangeGenerator.allRemaining());
    }


}