package u02lab.code;

/**
 * Created by margherita on 07/03/17.
 */
public interface GeneratorFactory {

    SequenceGenerator createRandomGenerator(int n);

    SequenceGenerator createRangeGenerator(int start, int stop);

}
