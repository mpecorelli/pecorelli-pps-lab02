package u02lab.code;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeStrategy. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomStrategy vs u02lab.code.RangeStrategy)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomStrategy implements GeneratorStrategy {

    private static final double THRESHOLD = 0.5;

    private final int n;

    public RandomStrategy(final int n){
        this.n = n;
    }

    @Override
    public Integer getNext() {
        if(Math.random() >= THRESHOLD) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Integer getStart() {
        return 0;
    }

    @Override
    public Integer getStop() {
        return this.n - 1;
    }
}
