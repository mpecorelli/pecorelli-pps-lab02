package u02lab.code;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeStrategy implements GeneratorStrategy {

    private int currentNumber;
    private final int start;
    private final int stop;

    public RangeStrategy(final int start, final int stop){
        this.currentNumber = start;
        this.start = start;
        this.stop = stop;

    }

    @Override
    public Integer getNext() {
        return this.currentNumber++;
    }

    @Override
    public Integer getStart() {
        return this.start;
    }

    @Override
    public Integer getStop() {
        return this.stop;
    }
}
