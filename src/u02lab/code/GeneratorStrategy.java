package u02lab.code;

import java.util.Optional;

/**
 * Created by margherita on 07/03/17.
 */
public interface GeneratorStrategy {

    Integer getNext();

    Integer getStart();

    Integer getStop();

}
