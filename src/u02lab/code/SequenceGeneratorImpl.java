package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by margherita on 07/03/17.
 */
public class SequenceGeneratorImpl implements SequenceGenerator {

    private List<Integer> randomBits;

    private final int range;
    private final GeneratorStrategy strategy;

    private int counter;

    public SequenceGeneratorImpl(final GeneratorStrategy strategy){
        this.range = strategy.getStop() - strategy.getStart();
        this.strategy = strategy;

        this.randomBits = new ArrayList();
        for(int i = strategy.getStart(); i <= strategy.getStop(); i++) {
            randomBits.add(strategy.getNext());
        }
    }

    @Override
    public Optional<Integer> next() {
        if(this.counter <= this.range) {
            return Optional.of(this.randomBits.get(this.counter++));
        }
        return Optional.empty();


    }

    @Override
    public void reset() {
        this.counter = 0;

    }

    @Override
    public boolean isOver() {
        return this.counter > this.range;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remainingBis = new ArrayList<>();
        for(int i = this.counter; i <= this.range; i++) {
            remainingBis.add(this.randomBits.get(i));
        }
        return Collections.unmodifiableList(remainingBis);
    }
}
