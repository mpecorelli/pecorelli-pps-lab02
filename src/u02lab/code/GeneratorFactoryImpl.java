package u02lab.code;

/**
 * Created by margherita on 07/03/17.
 */
public class GeneratorFactoryImpl implements GeneratorFactory {
    @Override
    public SequenceGenerator createRandomGenerator(final int n) {
        return new SequenceGeneratorImpl(new RandomStrategy(n));
    }

    @Override
    public SequenceGenerator createRangeGenerator(final int start, final int stop) {
        return new SequenceGeneratorImpl(new RangeStrategy(start, stop));
    }
}
